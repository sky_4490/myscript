#!/usr/bin/env python

import rospy
import random
from geometry_msgs.msg import Twist

pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=1000)
rospy.init_node('turtle_publisher_node')
vel_msg = Twist()
r = rospy.Rate(1000) #10Hz


while not rospy.is_shutdown():
	rospy.loginfo("Sending random velocity command:")
##	vel_msg.linear.x=2
##	vel_msg.angular.z=2
	vel_msg.linear.x = 2*random.randint(-2,4)
	vel_msg.angular.z = 4*random.randint(-10,10)
        rospy.loginfo("Sending random velocity command: \n")
	rospy.loginfo("linear.x = %d , angular.z= %d",vel_msg.linear.x , vel_msg.angular.z)

	pub.publish(vel_msg)
	r.sleep()

